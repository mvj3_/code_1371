private void startAnimationPopOut() {         
    LinearLayout myLayout = (LinearLayout) findViewById(R.id.anim_layout);

    Animation animation = AnimationUtils.loadAnimation(this,R.anim.bottom_out);

    animation.setAnimationListener(new AnimationListener() {                  
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {

        }
    });

    myLayout.clearAnimation();
    myLayout.startAnimation(animation);

}